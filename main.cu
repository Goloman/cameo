#include "helpers/utils.h"
#include "helpers/imgui_impl_sdl_gl3.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <limits>
#include <vector>
#include <fstream>
#include <sstream>

struct Renderer {
	SDL_GLContext context;
	SDL_Window *window;
};

struct Material {
	float *data;
	int xcount, ycount;
	float xsize, ysize;
	float height;
	float safeHeight;
};

struct Path {
	std::vector<glm::vec3> positions;
	std::vector<bool> downFlag;
	std::vector<int> downwards;
	float radius;
	bool cylinder;
};

int FindViolation(Path p, Material m) {
	for (int i = 0; i < p.positions.size(); i++) {
		if (p.positions[i].z < m.safeHeight) return i+1;
	}
	return 0;
}

Path CreatePath(const char *name) {
	Path ret;
	const char *seeker = name;

	ret.radius = 0;
	ret.cylinder = false;

	while(*seeker) seeker++;

	if (seeker - name < 4) return ret;

	seeker -= 3;
	ret.cylinder = *seeker == 'f';
	seeker++;
	float radius;
	radius = (*seeker - '0') * 10;
	seeker++;
	radius += *seeker - '0';
	radius /= 2;
	ret.radius = radius;


	int n, g;
	float x,y,z;

	FILE *file = fopen(name, "r");
	if (!file) return ret;

	float last_z = -std::numeric_limits<float>::infinity();
	int index = 0;
	while (!feof(file)) {
		fscanf(file, "N%dG%dX%fY%fZ%f\n", &n, &g, &x, &y, &z);
		ret.positions.push_back(glm::vec3(-x,y,z));
		if (z < last_z) {
			ret.downwards.push_back(index);
		}
		ret.downFlag.push_back(z < last_z);
		last_z = z;
		index++;
	}

	fclose(file);

	return ret;
}

Material CreateMaterial(int xcount, int ycount, float xsize, float ysize, float height, float safeHeight) {
	Material ret;
	ret.xsize = xsize;
	ret.ysize = ysize;
	ret.height = height;
	ret.safeHeight = safeHeight;
	cudaMallocManaged(&ret.data, sizeof(float) * xcount * ycount);
	if (ret.data) {
		ret.xcount = xcount;
		ret.ycount = ycount;
		for (int i = 0; i < xcount * ycount; i++) {
			ret.data[i] = height;
		}
	} else {
		ret.xcount = 0;
		ret.ycount = 0;
	}
	return ret;
}

void DestroyMaterial(Material m) {
	if (!m.data) return;
	cudaFree(m.data);
	m.data = 0;
}

void SaveImage(Material mat, const char* name) {
	int count = mat.xcount * mat.ycount;
	uint8_t *data2 = (uint8_t*)malloc(count * sizeof(*mat.data));
	for (int i = 0; i < count; i++) {
		data2[i] = mat.data[i] / mat.height * 255;
	}
	stbi_write_png(name, mat.xcount, mat.ycount, 1, data2, 0);
	free(data2);
}

__device__
__host__
float IntersectColumnCylinderPath(glm::vec2 column, glm::vec3 p0, glm::vec3 p1, float radius) {
	if (p0.z > p1.z) {
		auto temp = p0;
		p0 = p1;
		p1 = temp;
	}

	glm::vec3 c  = {column.x, column.y, 0};
	glm::vec3 c0 = {p0.x,     p0.y,     0};
	glm::vec3 c1 = {p1.x,     p1.y,     0};

	glm::vec3 n = glm::normalize(c1 - c0);
	glm::vec3 dir0 = glm::normalize(c - c0);
	glm::vec3 dir1 = glm::normalize(c - c1);

	if (glm::dot(n,dir0) < 0 && glm::length(c0 - c) >= radius) return std::numeric_limits<float>::infinity();
	if (glm::dot(-n,dir1) < 0 && glm::length(c1 - c) >= radius) return std::numeric_limits<float>::infinity();

	float dist = glm::length(glm::cross(n, c - c0));

	if (dist >= radius) return std::numeric_limits<float>::infinity();

	if (glm::length(c0 - c) < radius) return p0.z;

	float a = sqrt(radius * radius - dist * dist);

	glm::vec3 side = glm::cross(n, glm::vec3(0, 0, 1));
	glm::vec3 offset = side * dist;
	offset += a * n;
	
	glm::vec3 corrected_c = c - offset;

	float t = glm::length(corrected_c - c0) / glm::length(c1 - c0);

	return t * p1.z + (1 - t) * p0.z;
}

__device__
__host__
float IntersectColumnBallPath(glm::vec2 column, glm::vec3 p0, glm::vec3 p1, float radius) {
	if (p0.z > p1.z) {
		auto temp = p0;
		p0 = p1;
		p1 = temp;
	}

	glm::vec3 c  = {column.x, column.y, 0};
	glm::vec3 c0 = {p0.x,     p0.y,     0};
	glm::vec3 c1 = {p1.x,     p1.y,     0};

	glm::vec3 n = glm::normalize(c1 - c0);
	glm::vec3 normal = glm::normalize(p1 - p0);
	glm::vec3 down = glm::normalize(glm::cross(normal, glm::normalize(glm::cross(n, glm::vec3(0, 0, 1)))));
	glm::vec3 dir0 = glm::normalize(c - c0);
	glm::vec3 dir1 = glm::normalize(c - c1);

	if (glm::dot(n,dir0) < 0 && glm::length(c0 - c) >= radius) return std::numeric_limits<float>::infinity();
	if (glm::dot(-n,dir1) < 0 && glm::length(c1 - c) >= radius) return std::numeric_limits<float>::infinity();

	float dist = glm::length(glm::cross(n, c - c0));

	if (dist >= radius) return std::numeric_limits<float>::infinity();

	float minimum = std::numeric_limits<float>::infinity();

	float d0 = glm::length(c0 - c);
	if (d0 < radius) {
		float dist = sqrt(radius * radius - d0 * d0);
		dist = p0.z - dist;
		if (dist < minimum) minimum = dist;
	};

	float d1 = glm::length(c1 - c);
	if (d1 < radius) {
		float dist = sqrt(radius * radius - d1 * d1);
		dist = p1.z - dist;
		if (dist < minimum) minimum = dist;
	};

	float a = sqrt(radius * radius - dist * dist);
	glm::vec3 side = glm::cross(normal, glm::vec3(0, 0, 1));
	glm::vec3 offset = side * dist;
	//if (glm::dot(side, dir0) < 0) offset = -offset;
	offset += a * down;

	glm::vec3 offset2 = glm::vec3(offset.x, offset.y, 0);
	
	glm::vec3 corrected_c = c - offset2;

	float t = glm::dot(corrected_c - c0, c1 - c0) / glm::length(c1 - c0) / glm::length(c1 - c0);

	if (t > 1 || t < 0) return minimum + radius;

	glm::vec3 origin = t * p1 + (1-t) * p0;

	float candidate =  origin.z + offset.z;
	if (candidate < minimum) minimum = candidate;

	return minimum + radius;
}

__global__
void UpdateCylinder(Material m, glm::vec3 p0, glm::vec3 p1, float radius) {
	int width = m.xcount;
	int height = m.ycount;
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % width;
	int y = index / width;
	float xs = x - (width-1)  / 2.f;
	float ys = y - (height-1) / 2.f;
	float xcoord = xs * m.xsize / (width-1);
	float ycoord = ys * m.ysize / (height-1);
	float updated = IntersectColumnCylinderPath(glm::vec2(xcoord, ycoord), p0, p1, radius);
	atomicMin((int*)(m.data+index), *((int*)&updated));
}

__global__
void UpdateCylinderCheck(Material m, glm::vec3 p0, glm::vec3 p1, float radius, bool* flag) {
	int width = m.xcount;
	int height = m.ycount;
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	bool swap = false;

	if (index < width * height) {
		int x = index % width;
		int y = index / width;
		float xs = x - (width-1)  / 2.f;
		float ys = y - (height-1) / 2.f;
		float xcoord = xs * m.xsize / (width-1);
		float ycoord = ys * m.ysize / (height-1);
		float updated = IntersectColumnCylinderPath(glm::vec2(xcoord, ycoord), p0, p1, radius);
		swap = updated < m.data[index];
		atomicMin((int*)(m.data+index), *((int*)&updated));
	}

	bool anySwap = __syncthreads_or(swap);
	if (threadIdx.x == 0) flag[blockIdx.x] = anySwap;
}

__global__
void UpdateBall(Material m, glm::vec3 p0, glm::vec3 p1, float radius) {
	int width = m.xcount;
	int height = m.ycount;
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % width;
	int y = index / width;
	float xs = x - (width-1)  / 2.f;
	float ys = y - (height-1) / 2.f;
	float xcoord = xs * m.xsize / (width-1);
	float ycoord = ys * m.ysize / (height-1);
	float updated = IntersectColumnBallPath(glm::vec2(xcoord, ycoord), p0, p1, radius);
	atomicMin((int*)(m.data+index), *((int*)&updated));
}

__global__
static void UpdateCylinder2(Material m, glm::vec3 p0, glm::vec3 p1, float radius, glm::ivec2 start, glm::ivec2 size) {
	int width = m.xcount;
	int height = m.ycount;
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % size.x + start.x;
	int y = index / size.x + start.y;
	if (y >= height) return;
	float xs = x - (width-1)  / 2.f;
	float ys = y - (height-1) / 2.f;
	float xcoord = xs * m.xsize / (width-1);
	float ycoord = ys * m.ysize / (height-1);
	float updated = IntersectColumnCylinderPath(glm::vec2(xcoord, ycoord), p0, p1, radius);
	int offset = y * width + x;
	atomicMin((int*)(m.data+offset), *((int*)&updated));
}

__global__
static void UpdateBall2(Material m, glm::vec3 p0, glm::vec3 p1, float radius, glm::ivec2 start, glm::ivec2 size) {
	int width = m.xcount;
	int height = m.ycount;
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % size.x + start.x;
	int y = index / size.x + start.y;
	if (y >= height) return;
	float xs = x - (width-1)  / 2.f;
	float ys = y - (height-1) / 2.f;
	float xcoord = xs * m.xsize / (width-1);
	float ycoord = ys * m.ysize / (height-1);
	float updated = IntersectColumnBallPath(glm::vec2(xcoord, ycoord), p0, p1, radius);
	int offset = y * width + x;
	atomicMin((int*)(m.data+offset), *((int*)&updated));
}

GLuint LoadShaders(const char* vertex_file_path, const char* geometry_file_path, const char* fragment_file_path) {
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint GeometryShaderID = glCreateShader(GL_GEOMETRY_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	} else {
		printf("Can't open %s\n", vertex_file_path);
		return 0;
	}

	std::string GeometryShaderCode;
	std::ifstream GeometryShaderStream(geometry_file_path, std::ios::in);
	if (GeometryShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << GeometryShaderStream.rdbuf();
		GeometryShaderCode = sstr.str();
		GeometryShaderStream.close();
	} else {
		printf("Can't open %s\n", geometry_file_path);
		return 0;
	}

	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	} else {
		printf("Can't open %s\n", fragment_file_path);
		return 0;
	}

	GLint result = GL_FALSE;
	int InfoLogLength;

	auto VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> Error(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &Error[0]);
		printf("%s\n", &Error[0]);
	}

	auto GeometrySourcePointer = GeometryShaderCode.c_str();
	glShaderSource(GeometryShaderID, 1, &GeometrySourcePointer, NULL);
	glCompileShader(GeometryShaderID);
	glGetShaderiv(GeometryShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(GeometryShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> Error(InfoLogLength+1);
		glGetShaderInfoLog(GeometryShaderID, InfoLogLength, NULL, &Error[0]);
		printf("%s\n", &Error[0]);
	}

	auto FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> Error(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &Error[0]);
		printf("%s\n", &Error[0]);
	}

	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, GeometryShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	glGetProgramiv(ProgramID, GL_LINK_STATUS, &result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> Error(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &Error[0]);
		printf("%s\n", &Error[0]);
	}

	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, GeometryShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(GeometryShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

static glm::ivec2 SpaceToPic(Material m, glm::vec2 space) { 
	int width = m.xcount;
	int height = m.ycount;
	float xc = space.x;
	float yc = space.y;
	float xs = xc * (width - 1) / m.xsize;
	float ys = yc * (height - 1) / m.ysize;
	int x = xs + (width - 1) / 2.f;
	int y = ys + (height - 1) / 2.f;
	return {x, y};
}


void IntersectPath(Material m, Path p) {
	if (p.positions.size() == 0) return;
	float radius = p.radius;
	for (int i = 0; i < p.positions.size() - 1; i++) {
		glm::vec3 p0 = p.positions[i];
		glm::vec3 p1 = p.positions[i + 1];
		float xstart = min(p0.x, p1.x) - radius;
		float xend = max(p0.x, p1.x) + radius;
		float ystart = min(p0.y, p1.y) - radius;
		float yend = max(p0.y, p1.y) + radius;
		glm::ivec2 start = SpaceToPic(m, glm::vec2(xstart, ystart));
		glm::ivec2 end = SpaceToPic(m, glm::vec2(xend, yend));
		if (start.x < 0) start.x = 0;
		if (start.y < 0) start.y = 0;
		if (end.x < 0) end.x = 0;
		if (end.y < 0) end.y = 0;
		if (start.x >= m.xcount) start.x = m.xcount - 1;
		if (start.y >= m.ycount) start.y = m.ycount - 1;
		if (end.x >= m.xcount) end.x = m.xcount - 1;
		if (end.y >= m.ycount) end.y = m.ycount - 1;
		glm::ivec2 size = end - start;
		size += glm::ivec2(1, 1);
		if (size.x <= 0 || size.y <= 0) continue;
		int count = size.x * size.y;
		int blockSize = 256;
		int blockCount = (count + blockSize - 1) / blockSize;
		if (p.cylinder) {
			UpdateCylinder2<<<blockCount, blockSize>>>(m, p0, p1, radius, start, size);
		} else {
			UpdateBall2<<<blockCount, blockSize>>>(m, p0, p1, radius, start, size);
		}
	}
	cudaDeviceSynchronize();
}

int main(int argc, char** argv)
{
	const int filenameLength = 128;
	char matcapName[filenameLength] = "matcaps/default.jpg";
	char pathName[filenameLength] = "";

	float userSpeed = 15;
	float speed = userSpeed * 1000 / 60.f;

	bool milling = false;
	int currentIdx = 0;
	float partial = 0;

	bool showPath = false;

	int safeHeightErrorLine = 0;
	int downwardsMillingErrorLine = 0;
	int currentModalLine = 0;
	bool performChecks = true;

	Path path;
	path.radius = 8;
	path.cylinder = false;

	Renderer renderer;
	renderer.context = ImGui_SDLGL3_Init("pśw", true, &renderer.window);
	ImGui::CreateContext();
	ImGui_ImplSdlGL3_Init(renderer.window);
	ImGui::StyleColorsClassic();

	GLuint materialProgram = LoadShaders("matVS.glsl", "matGS.glsl", "matFS.glsl");
	GLuint simpleProgram = LoadShaders("simpleVS.glsl", "simpleFS.glsl");

	bool running = true;

	glm::vec3 cameraPos = {0, 80, 100};
	glm::vec2 cameraRot = {0.5, 0};

	int pattern_xcount = 1024 * 3;
	int pattern_ycount = pattern_xcount;
	float pattern_xsize = 150;
	float pattern_ysize = 150;
	float pattern_height = 50;
	float pattern_safeHeight = 20;

	Material mat = CreateMaterial(pattern_xcount, pattern_ycount, pattern_xsize, pattern_ysize, pattern_height, pattern_safeHeight);
	if (!mat.data) {
		printf("Out of memory\n");
		return 1;
	}

	glm::vec3 p0;
	glm::vec3 p1;

	GLuint lonelyVao;
	glGenVertexArrays(1, &lonelyVao);
	glBindVertexArray(lonelyVao);

	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glEnableVertexAttribArray(10);

	glEnable(GL_TEXTURE_2D);

	GLuint textureID;
	glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	GLuint matcapID;
	glGenTextures(1, &matcapID);
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, matcapID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	{
		glActiveTexture(GL_TEXTURE0 + 1);
		int x,y,n;
		unsigned char *data = stbi_load(matcapName, &x, &y, &n, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, n == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, data);
		stbi_image_free(data);
	}

	cudaDeviceSynchronize();

	ImGuiIO& io = ImGui::GetIO();

	while (running) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			ImGui_ImplSdlGL3_ProcessEvent(&event);
			if (event.type == SDL_QUIT) {
				running = false;
			}
			if (event.type == SDL_KEYDOWN) {
				switch(event.key.keysym.sym) {
				case SDLK_ESCAPE:
					running = false;
					break;
				}
			}
		}

		ImGui_ImplSdlGL3_NewFrame(renderer.window);

		glm::vec3 front = {sin(cameraRot.y), 0, -cos(cameraRot.y)};
		glm::vec3 side = {cos(cameraRot.y), 0, sin(cameraRot.y)};
		glm::vec3 up = {0, 1, 0};

		if (!io.WantCaptureKeyboard) {
			const Uint8* keyboardState = SDL_GetKeyboardState(0);
			if (keyboardState[SDL_SCANCODE_W])
				cameraPos += front * io.DeltaTime * 40.f;
			if (keyboardState[SDL_SCANCODE_S])
				cameraPos -= front * io.DeltaTime * 40.f;
			if (keyboardState[SDL_SCANCODE_D])
				cameraPos += side * io.DeltaTime * 40.f;
			if (keyboardState[SDL_SCANCODE_A])
				cameraPos -= side * io.DeltaTime * 40.f;
			if (keyboardState[SDL_SCANCODE_LSHIFT])
				cameraPos += up * io.DeltaTime * 40.f;
			if (keyboardState[SDL_SCANCODE_LCTRL])
				cameraPos -= up * io.DeltaTime * 40.f;
			if (keyboardState[SDL_SCANCODE_J])
				cameraRot.y -= io.DeltaTime;
			if (keyboardState[SDL_SCANCODE_L])
				cameraRot.y += io.DeltaTime;
			if (keyboardState[SDL_SCANCODE_I])
				cameraRot.x -= io.DeltaTime;
			if (keyboardState[SDL_SCANCODE_K])
				cameraRot.x += io.DeltaTime;
		}

		ImGui::Begin("Controls");
		ImGui::InputInt("X resolution", &pattern_xcount);
		ImGui::InputInt("Y resolution", &pattern_ycount);
		ImGui::InputFloat("X size", &pattern_xsize);
		ImGui::InputFloat("Y size", &pattern_ysize);
		ImGui::InputFloat("Height", &pattern_height);
		ImGui::InputFloat("Safe height", &pattern_safeHeight);
		if (ImGui::Button("New material")) {
			DestroyMaterial(mat);
			mat = CreateMaterial(pattern_xcount, pattern_ycount, pattern_xsize, pattern_ysize, pattern_height, pattern_safeHeight);
			milling = false;
			currentIdx = 0;
			partial = 0;
			safeHeightErrorLine = FindViolation(path, mat);
		}
		ImGui::Separator();
		ImGui::InputText("Matcap file", matcapName, filenameLength);
		if (ImGui::Button("Update matcap")) {
			glActiveTexture(GL_TEXTURE0 + 1);
			int x,y,n;
			unsigned char *data = stbi_load(matcapName, &x, &y, &n, 0);
			if (n)
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, n == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		ImGui::Separator();
		ImGui::InputText("Path file", pathName, filenameLength);
		ImGui::Text("Loaded positions: %zu", path.positions.size());
		if (ImGui::Button("Load file")) {
			path = CreatePath(pathName);
			safeHeightErrorLine = FindViolation(path, mat);
			milling = false;
			currentIdx = 0;
			partial = 0;
		}
		if (ImGui::Button("Reset")) {
			milling = false;
			currentIdx = 0;
			partial = 0;
		}
		ImGui::InputFloat("Speed", &userSpeed);
		speed = userSpeed * 1000 / 60.f;
		ImGui::Text("Current line: %d", currentIdx + 2);
		if (ImGui::Button("Toggle")) {
			milling = !milling;
		}
		ImGui::Checkbox("ShowPath", &showPath);
		ImGui::Text(milling ? "Running" : "Stopped");
		ImGui::Checkbox("Cylindrical", &path.cylinder);
		ImGui::InputFloat("Radius", &path.radius);
		if (ImGui::Button("TURBO")) {
			milling = false;
			partial = 0;
			IntersectPath(mat, path);
		}
		if (ImGui::Button("Fast forward")) {
			milling = false;
			partial = 0;
			float radius = path.radius;
			int count = mat.xcount * mat.ycount;
			int blockSize = 256;
			int blockCount = (count + blockSize - 1) / blockSize;
			if (path.cylinder && performChecks) {
				for (int i = currentIdx; i + 1 < path.positions.size(); i++) {
					p0 = path.positions[i];
					p1 = path.positions[i + 1];
					currentIdx = i+1;
					if (path.downFlag[i+1]) {
						bool *downFlag;
						cudaMallocManaged(&downFlag, sizeof(*downFlag) * blockCount);
						cudaDeviceSynchronize();
						for (int j = 0; j < blockCount; j++) downFlag[j] = false;
						UpdateCylinderCheck<<<blockCount, blockSize>>>(mat, p0, p1, radius, downFlag);
						cudaDeviceSynchronize();
						bool flag = false;
						for (int j = 0; j < blockCount; j++) flag = flag || downFlag[j];
						cudaFree(downFlag);
						if (flag) {
							downwardsMillingErrorLine = i + 2;;
							milling = false;
							break;
						}
					} else {
						UpdateCylinder<<<blockCount, blockSize>>>(mat, p0, p1, radius);
					}
				}
				cudaDeviceSynchronize();
			} else {
				for (int i = currentIdx; i + 1 < path.positions.size(); i++) {
					p0 = path.positions[i];
					p1 = path.positions[i + 1];
					currentIdx = i+1;
					if (path.cylinder) {
						UpdateCylinder<<<blockCount, blockSize>>>(mat, p0, p1, radius);
					} else {
						UpdateBall<<<blockCount, blockSize>>>(mat, p0, p1, radius);
					}
				}
				cudaDeviceSynchronize();
			}
		}
		ImGui::Checkbox("Perform checks", &performChecks);
		ImGui::End();

		if (safeHeightErrorLine) {
			currentModalLine = safeHeightErrorLine;
			safeHeightErrorLine = 0;
			ImGui::OpenPopup("Safe height error!");
		}

		if (downwardsMillingErrorLine) {
			currentModalLine = downwardsMillingErrorLine;
			downwardsMillingErrorLine = 0;
			ImGui::OpenPopup("Downwards milling error!");
		}

		if (ImGui::BeginPopupModal("Safe height error!", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
			ImGui::Text("Oh no! The milling bit goes below the safe height of %f on line %d!", mat.safeHeight, currentModalLine);
			if (ImGui::Button("Oh, well...")) {
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}

		if (ImGui::BeginPopupModal("Downwards milling error!", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
			ImGui::Text("Oh no! The flat milling bit removes material going downwards from line %d to line %d!", currentModalLine - 1, currentModalLine);
			if (ImGui::Button("Oh, well...")) {
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}

		if (milling) {
			int count = mat.xcount * mat.ycount;
			float radius = path.radius;
			int blockSize = 256;
			int blockCount = (count + blockSize - 1) / blockSize;
			float toDo = speed * io.DeltaTime;
			while (toDo > 0) {
				int startIdx = currentIdx;
				int endIdx = currentIdx + 1;
				if (endIdx >= path.positions.size()) {
					milling = false;
					break;
				}
				glm::vec3 p00 = path.positions[startIdx];
				glm::vec3 p1 = path.positions[endIdx];
				glm::vec3 p0 = partial * p1 + (1 - partial) * p00;
				float l = glm::length(p1 - p0);
				if (l >= toDo) {
					float t = toDo / l;
					p1 = t * p1 + (1 - t) * p0;
					toDo = 0;
					partial += (1 - partial) * t;
				} else {
					toDo -= l;
					currentIdx += 1;
					partial = 0;
				}
				if (path.cylinder) {
					if (performChecks && path.downFlag[endIdx]) {
						bool *downFlag;
						cudaMallocManaged(&downFlag, sizeof(*downFlag) * blockCount);
						cudaDeviceSynchronize();
						for (int i = 0; i < blockCount; i++) downFlag[i] = false;
						UpdateCylinderCheck<<<blockCount, blockSize>>>(mat, p0, p1, radius, downFlag);
						cudaDeviceSynchronize();
						bool flag = false;
						for (int i = 0; i < blockCount; i++) flag = flag || downFlag[i];
						cudaFree(downFlag);
						if (flag) {
							downwardsMillingErrorLine = endIdx + 1;;
							milling = false;
							break;
						}
					} else {
						UpdateCylinder<<<blockCount, blockSize>>>(mat, p0, p1, radius);
					}
				} else {
					UpdateBall<<<blockCount, blockSize>>>(mat, p0, p1, radius);
				}
			}
			cudaDeviceSynchronize();
		}

		auto perspective = glm::perspective(glm::radians(45.f), io.DisplaySize.x / (float) io.DisplaySize.y, 1.f, 1000.f); 
		auto view = glm::mat4(1);
		view = glm::rotate(view, cameraRot.x, {1, 0, 0});
		view = glm::rotate(view, cameraRot.y, {0, 1, 0});
		view *= glm::translate(-cameraPos);

		glClearColor(0.f, 0.f, 0.f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(materialProgram);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		glUniform1i(0, mat.xcount - 1);
		glUniform1i(1, mat.ycount - 1);
		glUniform1f(2, mat.xsize);
		glUniform1f(3, mat.ysize);
		glUniform1i(4, 0);
		glUniform1i(7, 1);
		glUniformMatrix4fv(5, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(6, 1, GL_FALSE, glm::value_ptr(perspective));

		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, mat.xcount, mat.ycount, 0, GL_RED, GL_FLOAT, mat.data);

		glDrawArrays(GL_POINTS, 0, (mat.xcount + 1) * (mat.ycount + 1) - 3);

		glUseProgram(simpleProgram);
		glClear(GL_DEPTH_BUFFER_BIT);

		glUniformMatrix4fv(5, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(6, 1, GL_FALSE, glm::value_ptr(perspective));

		if (showPath) {
			glBufferData(GL_ARRAY_BUFFER, path.positions.size() * sizeof(glm::vec3), &path.positions[0], GL_STREAM_DRAW);
			glVertexAttribPointer(10, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);
			glUniform3f(8, 1, 0, 0);
			glDrawArrays(GL_LINE_STRIP, 0, path.positions.size());
		}

		if (path.positions.size()) {
			glPointSize(9);
			glm::vec3 bitPosition = path.positions[currentIdx];
			if (partial > 0) {
				bitPosition *= 1 - partial;
				bitPosition += path.positions[currentIdx + 1] * partial;
			}
			glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3), &bitPosition.x, GL_STREAM_DRAW);
			glVertexAttribPointer(10, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);
			glUniform3f(8, 0, 1, 0);
			glDrawArrays(GL_POINTS, 0, 1);
		}

		ImGui::Render();
		ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
		SDL_GL_SwapWindow(renderer.window);
	}

	DestroyMaterial(mat);
}

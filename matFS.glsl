#version 330 core

#extension GL_ARB_explicit_uniform_location : enable

in float height;
in vec3 normal;
in vec3 position;

out vec4 outColor;

layout (location = 7) uniform sampler2D matcap;

void main() {
	vec3 n = normalize(normal);
	n *= 0.95;
	//if (dot(n, -position) < 0) n = -n;
	vec2 tc = ((n.xy * vec2(1, -1)) + vec2(1,1)) / 2;
	outColor = texture(matcap, tc);
	//outColor = vec4(n,1);
}

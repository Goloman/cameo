#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout (location = 8) uniform vec3 color;

out vec4 out_color;

void main() {
	out_color = vec4(color, 1);
}

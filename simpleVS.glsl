#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout (location = 5) uniform mat4 view;
layout (location = 6) uniform mat4 perspective;

layout (location = 10) in vec3 pos;

void main() {
	gl_Position = perspective * view * vec4(pos.x, pos.z, pos.y, 1);
}

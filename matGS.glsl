#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

layout (location = 0) uniform int xcount;
layout (location = 1) uniform int ycount;
layout (location = 2) uniform float xsize;
layout (location = 3) uniform float ysize;
layout (location = 4) uniform sampler2D heightmap;
layout (location = 5) uniform mat4 view;
layout (location = 6) uniform mat4 perspective;

in int index[];

out float height;
out vec3 normal;
out vec3 position;

vec2 indicesToUv(int x, int y) {
	float u = float(x) / xcount;
	float v = float(y) / ycount;
	return vec2(u,v);
}

void makeVertex(int x, int y) {
	float ustep = 1.f / xcount;
	float vstep = 1.f / ycount;

	vec2 uv = indicesToUv(x, y);
	vec2 pos = uv - vec2(0.5, 0.5);
	pos *= vec2(xsize, ysize);
	height = texture(heightmap, uv).r;
	position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
	gl_Position = perspective * vec4(position, 1);

	vec2 offsets[4];
	offsets[0] = uv + vec2(-ustep, 0);
	offsets[1] = uv + vec2(ustep, 0);
	offsets[2] = uv + vec2(0, -vstep);
	offsets[3] = uv + vec2(0, vstep);
	float heights[4];
	for (int i = 0; i < 4; i++) {
		heights[i] = texture(heightmap, offsets[i]).r;
	}
	vec3 a = normalize(vec3(2 * ustep * xsize, heights[1] - heights[0], 0));
	vec3 b = normalize(vec3(0, heights[3] - heights[2], 2 * vstep * ysize));
	normal = normalize((view * vec4(cross(b,a).xyz, 0)).xyz);

	EmitVertex();
}



void side(int idx) {
	int b = idx / xcount;
	int a;
	if (b < 2) {
		a = idx % xcount;
	} else {
		idx -= xcount * 2;
		b = idx / ycount + 2;
		a = idx % ycount;
	}

	float ustep = 1.f / xcount;
	float vstep = 1.f / ycount;

	switch(b) {
	case 0:
		normal = (view * vec4(0, 0, -1, 0)).xyz;
		{
			vec2 uv = indicesToUv(a, 0);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		{
			vec2 uv = indicesToUv(a+1, 0);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		EndPrimitive();
	case 1:
		normal = (view * vec4(0, 0, 1, 0)).xyz;
		{
			vec2 uv = indicesToUv(a, ycount);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		{
			vec2 uv = indicesToUv(a+1, ycount);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		EndPrimitive();
	case 2:
		normal = (view * vec4(-1, 0, 0, 0)).xyz;
		{
			vec2 uv = indicesToUv(0, a);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		{
			vec2 uv = indicesToUv(0, a+1);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		EndPrimitive();
	case 3:
		normal = (view * vec4(1, 0, 0, 0)).xyz;
		{
			vec2 uv = indicesToUv(xcount, a);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		{
			vec2 uv = indicesToUv(xcount, a+1);
			vec2 pos = uv - vec2(0.5, 0.5);
			pos *= vec2(xsize, ysize);
			height = texture(heightmap, uv).r;
			position = (view * vec4(pos.x, height, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
			position = (view * vec4(pos.x, 0, pos.y, 1)).xyz;
			gl_Position = perspective * vec4(position, 1);
			EmitVertex();
		}
		EndPrimitive();
	case 4:
		normal = (view * vec4(0, -1, 0, 0)).xyz;
		gl_Position = perspective * view * vec4(xsize / 2, 0, ysize/2, 1);
		EmitVertex();
		gl_Position = perspective * view * vec4(xsize / 2, 0, -ysize/2, 1);
		EmitVertex();
		gl_Position = perspective * view * vec4(-xsize / 2, 0, ysize/2, 1);
		EmitVertex();
		gl_Position = perspective * view * vec4(-xsize / 2, 0, -ysize/2, 1);
		EmitVertex();
		EndPrimitive();
		return;
	}
	
}

void main() {
	int count = xcount * ycount;
	if (index[0] >= count) {
		side(index[0] - count);
		return;
	}

	int x0 = index[0] % xcount;
	int y0 = index[0] / xcount;
	int x1 = x0 + 1;
	int y1 = y0 + 1;

	makeVertex(x0, y0);
	makeVertex(x0, y1);
	makeVertex(x1, y0);
	makeVertex(x1, y1);
	EndPrimitive();
}
